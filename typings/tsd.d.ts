interface IFormItem {
  // Form item name, which should unique for each item in a form
  name?: string;

  // Form item type
  type: 'text' | 'date' | 'button';

  // Whether the item is required or not, false by default
  required?: boolean;

  // Form item label
  label?: string;

  // Date format for date item
  format?: string;

  // Placeholder
  placeholder?: string;

  // Button's title
  title?: string;

  // Expands the button to 100% of available space
  block?: boolean;

  // Describe if this is the Submit button
  submit?: boolean;

  // Function to be triggered when the button is pressed
  callback?: any;
}

interface IReservation {
  id: string;
  name: string;
  hotelName: string;
  arrivalDate: string;
  departureDate: string;
}
