// tslint:disable
// graphql typescript definitions

declare namespace GQL {
  interface IGraphQLResponseRoot {
    data?: IQuery | IMutation | ISubscription;
    errors?: Array<IGraphQLResponseError>;
  }

  interface IGraphQLResponseError {
    /** Required for all errors */
    message: string;
    locations?: Array<IGraphQLResponseErrorLocation>;
    /** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
    [propName: string]: any;
  }

  interface IGraphQLResponseErrorLocation {
    line: number;
    column: number;
  }

  interface IQuery {
    __typename: "Query";
    reservations: Array<IReservation | null>;
    reservation: IReservation | null;
    reservationsConnection: IReservationConnection;

    /**
     * Fetches an object given its ID
     */
    node: Node | null;
  }

  interface IReservationsOnQueryArguments {
    where?: IReservationWhereInput | null;
    orderBy?: ReservationOrderByInput | null;
    skip?: number | null;
    after?: string | null;
    before?: string | null;
    first?: number | null;
    last?: number | null;
  }

  interface IReservationOnQueryArguments {
    where: IReservationWhereUniqueInput;
  }

  interface IReservationsConnectionOnQueryArguments {
    where?: IReservationWhereInput | null;
    orderBy?: ReservationOrderByInput | null;
    skip?: number | null;
    after?: string | null;
    before?: string | null;
    first?: number | null;
    last?: number | null;
  }

  interface INodeOnQueryArguments {

    /**
     * The ID of an object
     */
    id: string;
  }

  interface IReservationWhereInput {

    /**
     * Logical AND on all given filters.
     */
    AND?: Array<IReservationWhereInput> | null;

    /**
     * Logical OR on all given filters.
     */
    OR?: Array<IReservationWhereInput> | null;

    /**
     * Logical NOT on all given filters combined by AND.
     */
    NOT?: Array<IReservationWhereInput> | null;
    id?: string | null;

    /**
     * All values that are not equal to given value.
     */
    id_not?: string | null;

    /**
     * All values that are contained in given list.
     */
    id_in?: Array<string> | null;

    /**
     * All values that are not contained in given list.
     */
    id_not_in?: Array<string> | null;

    /**
     * All values less than the given value.
     */
    id_lt?: string | null;

    /**
     * All values less than or equal the given value.
     */
    id_lte?: string | null;

    /**
     * All values greater than the given value.
     */
    id_gt?: string | null;

    /**
     * All values greater than or equal the given value.
     */
    id_gte?: string | null;

    /**
     * All values containing the given string.
     */
    id_contains?: string | null;

    /**
     * All values not containing the given string.
     */
    id_not_contains?: string | null;

    /**
     * All values starting with the given string.
     */
    id_starts_with?: string | null;

    /**
     * All values not starting with the given string.
     */
    id_not_starts_with?: string | null;

    /**
     * All values ending with the given string.
     */
    id_ends_with?: string | null;

    /**
     * All values not ending with the given string.
     */
    id_not_ends_with?: string | null;
    name?: string | null;

    /**
     * All values that are not equal to given value.
     */
    name_not?: string | null;

    /**
     * All values that are contained in given list.
     */
    name_in?: Array<string> | null;

    /**
     * All values that are not contained in given list.
     */
    name_not_in?: Array<string> | null;

    /**
     * All values less than the given value.
     */
    name_lt?: string | null;

    /**
     * All values less than or equal the given value.
     */
    name_lte?: string | null;

    /**
     * All values greater than the given value.
     */
    name_gt?: string | null;

    /**
     * All values greater than or equal the given value.
     */
    name_gte?: string | null;

    /**
     * All values containing the given string.
     */
    name_contains?: string | null;

    /**
     * All values not containing the given string.
     */
    name_not_contains?: string | null;

    /**
     * All values starting with the given string.
     */
    name_starts_with?: string | null;

    /**
     * All values not starting with the given string.
     */
    name_not_starts_with?: string | null;

    /**
     * All values ending with the given string.
     */
    name_ends_with?: string | null;

    /**
     * All values not ending with the given string.
     */
    name_not_ends_with?: string | null;
    hotelName?: string | null;

    /**
     * All values that are not equal to given value.
     */
    hotelName_not?: string | null;

    /**
     * All values that are contained in given list.
     */
    hotelName_in?: Array<string> | null;

    /**
     * All values that are not contained in given list.
     */
    hotelName_not_in?: Array<string> | null;

    /**
     * All values less than the given value.
     */
    hotelName_lt?: string | null;

    /**
     * All values less than or equal the given value.
     */
    hotelName_lte?: string | null;

    /**
     * All values greater than the given value.
     */
    hotelName_gt?: string | null;

    /**
     * All values greater than or equal the given value.
     */
    hotelName_gte?: string | null;

    /**
     * All values containing the given string.
     */
    hotelName_contains?: string | null;

    /**
     * All values not containing the given string.
     */
    hotelName_not_contains?: string | null;

    /**
     * All values starting with the given string.
     */
    hotelName_starts_with?: string | null;

    /**
     * All values not starting with the given string.
     */
    hotelName_not_starts_with?: string | null;

    /**
     * All values ending with the given string.
     */
    hotelName_ends_with?: string | null;

    /**
     * All values not ending with the given string.
     */
    hotelName_not_ends_with?: string | null;
    arrivalDate?: string | null;

    /**
     * All values that are not equal to given value.
     */
    arrivalDate_not?: string | null;

    /**
     * All values that are contained in given list.
     */
    arrivalDate_in?: Array<string> | null;

    /**
     * All values that are not contained in given list.
     */
    arrivalDate_not_in?: Array<string> | null;

    /**
     * All values less than the given value.
     */
    arrivalDate_lt?: string | null;

    /**
     * All values less than or equal the given value.
     */
    arrivalDate_lte?: string | null;

    /**
     * All values greater than the given value.
     */
    arrivalDate_gt?: string | null;

    /**
     * All values greater than or equal the given value.
     */
    arrivalDate_gte?: string | null;

    /**
     * All values containing the given string.
     */
    arrivalDate_contains?: string | null;

    /**
     * All values not containing the given string.
     */
    arrivalDate_not_contains?: string | null;

    /**
     * All values starting with the given string.
     */
    arrivalDate_starts_with?: string | null;

    /**
     * All values not starting with the given string.
     */
    arrivalDate_not_starts_with?: string | null;

    /**
     * All values ending with the given string.
     */
    arrivalDate_ends_with?: string | null;

    /**
     * All values not ending with the given string.
     */
    arrivalDate_not_ends_with?: string | null;
    departureDate?: string | null;

    /**
     * All values that are not equal to given value.
     */
    departureDate_not?: string | null;

    /**
     * All values that are contained in given list.
     */
    departureDate_in?: Array<string> | null;

    /**
     * All values that are not contained in given list.
     */
    departureDate_not_in?: Array<string> | null;

    /**
     * All values less than the given value.
     */
    departureDate_lt?: string | null;

    /**
     * All values less than or equal the given value.
     */
    departureDate_lte?: string | null;

    /**
     * All values greater than the given value.
     */
    departureDate_gt?: string | null;

    /**
     * All values greater than or equal the given value.
     */
    departureDate_gte?: string | null;

    /**
     * All values containing the given string.
     */
    departureDate_contains?: string | null;

    /**
     * All values not containing the given string.
     */
    departureDate_not_contains?: string | null;

    /**
     * All values starting with the given string.
     */
    departureDate_starts_with?: string | null;

    /**
     * All values not starting with the given string.
     */
    departureDate_not_starts_with?: string | null;

    /**
     * All values ending with the given string.
     */
    departureDate_ends_with?: string | null;

    /**
     * All values not ending with the given string.
     */
    departureDate_not_ends_with?: string | null;
  }

  const enum ReservationOrderByInput {
    id_ASC = 'id_ASC',
    id_DESC = 'id_DESC',
    name_ASC = 'name_ASC',
    name_DESC = 'name_DESC',
    hotelName_ASC = 'hotelName_ASC',
    hotelName_DESC = 'hotelName_DESC',
    arrivalDate_ASC = 'arrivalDate_ASC',
    arrivalDate_DESC = 'arrivalDate_DESC',
    departureDate_ASC = 'departureDate_ASC',
    departureDate_DESC = 'departureDate_DESC',
    updatedAt_ASC = 'updatedAt_ASC',
    updatedAt_DESC = 'updatedAt_DESC',
    createdAt_ASC = 'createdAt_ASC',
    createdAt_DESC = 'createdAt_DESC'
  }

  interface IReservation {
    __typename: "Reservation";
    id: string;
    name: string;
    hotelName: string;
    arrivalDate: string;
    departureDate: string;
  }

  /**
   * An object with an ID
   */
  type Node = IReservation;

  /**
   * An object with an ID
   */
  interface INode {
    __typename: "Node";

    /**
     * The id of the object.
     */
    id: string;
  }

  interface IReservationWhereUniqueInput {
    id?: string | null;
  }

  /**
   * A connection to a list of items.
   */
  interface IReservationConnection {
    __typename: "ReservationConnection";

    /**
     * Information to aid in pagination.
     */
    pageInfo: IPageInfo;

    /**
     * A list of edges.
     */
    edges: Array<IReservationEdge | null>;
    aggregate: IAggregateReservation;
  }

  /**
   * Information about pagination in a connection.
   */
  interface IPageInfo {
    __typename: "PageInfo";

    /**
     * When paginating forwards, are there more items?
     */
    hasNextPage: boolean;

    /**
     * When paginating backwards, are there more items?
     */
    hasPreviousPage: boolean;

    /**
     * When paginating backwards, the cursor to continue.
     */
    startCursor: string | null;

    /**
     * When paginating forwards, the cursor to continue.
     */
    endCursor: string | null;
  }

  /**
   * An edge in a connection.
   */
  interface IReservationEdge {
    __typename: "ReservationEdge";

    /**
     * The item at the end of the edge.
     */
    node: IReservation;

    /**
     * A cursor for use in pagination.
     */
    cursor: string;
  }

  interface IAggregateReservation {
    __typename: "AggregateReservation";
    count: number;
  }

  interface IMutation {
    __typename: "Mutation";
    createReservation: IReservation;
    updateReservation: IReservation | null;
    deleteReservation: IReservation | null;
    upsertReservation: IReservation;
    updateManyReservations: IBatchPayload;
    deleteManyReservations: IBatchPayload;
  }

  interface ICreateReservationOnMutationArguments {
    data: IReservationCreateInput;
  }

  interface IUpdateReservationOnMutationArguments {
    data: IReservationUpdateInput;
    where: IReservationWhereUniqueInput;
  }

  interface IDeleteReservationOnMutationArguments {
    where: IReservationWhereUniqueInput;
  }

  interface IUpsertReservationOnMutationArguments {
    where: IReservationWhereUniqueInput;
    create: IReservationCreateInput;
    update: IReservationUpdateInput;
  }

  interface IUpdateManyReservationsOnMutationArguments {
    data: IReservationUpdateManyMutationInput;
    where?: IReservationWhereInput | null;
  }

  interface IDeleteManyReservationsOnMutationArguments {
    where?: IReservationWhereInput | null;
  }

  interface IReservationCreateInput {
    id?: string | null;
    name: string;
    hotelName: string;
    arrivalDate: string;
    departureDate: string;
  }

  interface IReservationUpdateInput {
    name?: string | null;
    hotelName?: string | null;
    arrivalDate?: string | null;
    departureDate?: string | null;
  }

  interface IReservationUpdateManyMutationInput {
    name?: string | null;
    hotelName?: string | null;
    arrivalDate?: string | null;
    departureDate?: string | null;
  }

  interface IBatchPayload {
    __typename: "BatchPayload";

    /**
     * The number of nodes that have been affected by the Batch operation.
     */
    count: any;
  }

  interface ISubscription {
    __typename: "Subscription";
    reservation: IReservationSubscriptionPayload | null;
  }

  interface IReservationOnSubscriptionArguments {
    where?: IReservationSubscriptionWhereInput | null;
  }

  interface IReservationSubscriptionWhereInput {

    /**
     * Logical AND on all given filters.
     */
    AND?: Array<IReservationSubscriptionWhereInput> | null;

    /**
     * Logical OR on all given filters.
     */
    OR?: Array<IReservationSubscriptionWhereInput> | null;

    /**
     * Logical NOT on all given filters combined by AND.
     */
    NOT?: Array<IReservationSubscriptionWhereInput> | null;

    /**
     * The subscription event gets dispatched when it's listed in mutation_in
     */
    mutation_in?: Array<MutationType> | null;

    /**
     * The subscription event gets only dispatched when one of the updated fields names is included in this list
     */
    updatedFields_contains?: string | null;

    /**
     * The subscription event gets only dispatched when all of the field names included in this list have been updated
     */
    updatedFields_contains_every?: Array<string> | null;

    /**
     * The subscription event gets only dispatched when some of the field names included in this list have been updated
     */
    updatedFields_contains_some?: Array<string> | null;
    node?: IReservationWhereInput | null;
  }

  const enum MutationType {
    CREATED = 'CREATED',
    UPDATED = 'UPDATED',
    DELETED = 'DELETED'
  }

  interface IReservationSubscriptionPayload {
    __typename: "ReservationSubscriptionPayload";
    mutation: MutationType;
    node: IReservation | null;
    updatedFields: Array<string> | null;
    previousValues: IReservationPreviousValues | null;
  }

  interface IReservationPreviousValues {
    __typename: "ReservationPreviousValues";
    id: string;
    name: string;
    hotelName: string;
    arrivalDate: string;
    departureDate: string;
  }
}

// tslint:enable
