Reservation App
=======

Reservation App is a simple React Native project that works with the GraphQL backend.

## Setup
This project was bootstrapped with [react-native-cli](https://facebook.github.io/react-native/docs/getting-started#the-react-native-cli)

## Features
- Fetch latest 100 reservations from the [graphql-backend](https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev)

- Create a new reservation

## Tech stack
- [React Native](https://github.com/facebook/react-native)
- [Apollo Client](https://www.apollographql.com/docs/react/)
- [Jest](https://jestjs.io)
- [Typescript](https://facebook.github.io/react-native/blog/2018/05/07/using-typescript-with-react-native)

## Backend
https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev

Queries used in
- reservations query
- createReservation mutation

## Running the project
- [Install NodeJS](https://nodejs.org/en)

- Install react-native-cli globally
  > ```npm i -g react-native-cli```

- Run the project
  > ```react-native run-ios``` on iOS simulator

  > ```react-native run-android``` on a connected Android simulator or device

- Test the project
  > ```npm test```
