import { StyleSheet } from "react-native";
import colors from './colors';

// Global styles used in the app. Define styles useful for any component rather any specified one here.
export default function getGlobalStyles() {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center'
    },
    horizontal: {
      flexDirection: 'row'
    },
    vertical: {
      flexDirection: 'column'
    },
    h1: {
      color: colors.default,
      fontSize: 40,
      fontFamily: 'Lato-Bold',
      paddingTop: 20,
      paddingBottom: 20
    },
    h2: {
      color: colors.default,
      fontSize: 34,
      fontFamily: 'Lato-Bold',
      paddingTop: 17,
      paddingBottom: 17
    },
    h3: {
      color: colors.default,
      fontSize: 28,
      fontFamily: 'Lato-Bold',
      paddingTop: 14,
      paddingBottom: 14
    },
    h4: {
      color: colors.default,
      fontSize: 22,
      fontFamily: 'Lato-Bold',
      paddingTop: 11,
      paddingBottom: 11
    },
    h5: {
      color: colors.default,
      fontSize: 18,
      fontFamily: 'Lato-Bold',
      paddingTop: 9,
      paddingBottom: 9
    },
    subheading: {
      color: colors.default,
      fontSize: 16,
      fontFamily: 'Lato-Heavy',
      paddingTop: 8,
      paddingBottom: 8
    },
    bodyText: {
      color: colors.default,
      fontSize: 14,
      fontFamily: 'Lato-Regular',
      paddingTop: 7,
      paddingBottom: 7
    },
    caption: {
      color: colors.default,
      fontSize: 12,
      fontFamily: 'Lato-Regular',
      paddingTop: 6,
      paddingBottom: 6
    },
    block: {
      width: '100%'
    }
  })
}