// All color variables used in the app
export default {
  primary: '#1976D2',
  secondary: '#424242',
  accent: '#82B1FF',
  error: '#FF5252',
  info: '#2196F3',
  success: '#4CAF50',
  warning: '#FFC107',
  default: 'rgba(0,0,0,0.87)',
  grey: '#9E9E9E',
  darkGrey: '#616161',
  lightGrey: '#E0E0E0',
  white: '#FFFFFF'
};
