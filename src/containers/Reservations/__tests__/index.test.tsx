import React from 'react';
import renderer from 'react-test-renderer';
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';

import Reservations, { reservationsQuery } from '../index';

jest.mock('../../../routes', () => {
  return () => ({
    navigate: () => {},
    ROUTES: {
      Reservations: 'Reservations'
    }
  });
});

describe('Reservations container', () => {
  it('renders correctly', async () => {
    const reservations: Array<IReservation> = [
      {
        id: 'a1',
        name: 'Test One',
        hotelName: 'A',
        arrivalDate: '04/20/2019',
        departureDate: '04/21/2019'
      }, {
        id: 'a2',
        name: 'Test Two',
        hotelName: 'B',
        arrivalDate: '04/20/2019',
        departureDate: '04/21/2019'
      }
    ]
    const mocks = [
      {
        request: reservationsQuery,
        result: {
          data: {
            reservations
          }
        }
      }
    ]
    const component = renderer.create(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Reservations/>
      </MockedProvider>
    )
      .toJSON();

    await wait(0);

    expect(component).toMatchSnapshot();
  });
});
