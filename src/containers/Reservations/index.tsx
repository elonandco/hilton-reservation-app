import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Query, QueryResult } from 'react-apollo';
import { NavigationStackScreenOptions } from 'react-navigation';
import Loading from '../../components/common/Loading';
import ReservationsList from '../../components/ReservationsList';
import CreateReservationButton from '../../components/ReservationsList/CreateReservationButton';
import { getReservationsQuery } from '../../apollo/queries';
import styles from './styles';

// Declare this as an export variable to pass same query and variable in refetch when created new reservation (See containers/Reservations/index.tsx)
export const reservationsQuery = {
  query: getReservationsQuery,
  variables: {
    orderBy: 'createdAt_DESC',
    first: 100
  }
}

/**
 * @class Reservations
 * @description Reservations page component
 */
export default class Reservations extends Component {
  // Set navigation bar title and right component
  static navigationOptions = (): NavigationStackScreenOptions => {
    return {
      title: 'RESERVATIONS',
      headerRight: (<CreateReservationButton/>)
    }
  }

  render() {
    return (
      <Query
        query={reservationsQuery.query}
        variables={reservationsQuery.variables}
      >
        {({ loading, error, data}: QueryResult) => {
          if (loading) {
            return (
              <View style={styles.container}>
                <Loading title="Loading Reservations..."/>
              </View>
            );
          }
          if (error) {
            return (
              <View style={styles.container}>
                <Text>{error.message}</Text>
              </View>
            )
          }
          return (<ReservationsList items={data.reservations} />);
        }}
      </Query>
    )
  }
}
