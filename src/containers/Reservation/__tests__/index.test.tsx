import React from 'react';
import renderer from 'react-test-renderer';
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';

import Reservation from '../index';
import { createReservationMutation } from '../../../apollo/mutations';

jest.mock('../../../routes', () => {
  return () => ({
    navigate: () => {},
    ROUTES: {
      Reservations: 'Reservations'
    }
  });
});

describe('Reservation container', () => {
  it('renders correctly', async () => {
    const createReservation: IReservation = {
      id: 'the-uuid',
      name: 'Test One',
      hotelName: 'Hotel One',
      arrivalDate: '04/20/2019',
      departureDate: '04/21/2019'
    };
    const mocks = [
      {
        request: {
          query: createReservationMutation,
          variables: {
            name: 'Test One',
            hotelName: 'Hotel One',
            arrivalDate: '04/20/2019',
            departureDate: '04/21/2019'
          }
        },
        result: {
          data: {
            createReservation
          }
        }
      }
    ];

    const component = renderer.create(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Reservation/>
      </MockedProvider>
    )
      .toJSON();

    await wait(0);

    expect(component).toMatchSnapshot();
  });
});
