import React, {Component} from 'react';
import { Mutation } from 'react-apollo';
import { NavigationStackScreenOptions } from 'react-navigation';
import { createReservationMutation } from '../../apollo/mutations';
import { navigate, ROUTES } from '../../routes';
import ReservationDetail from '../../components/ReservationDetail';
import { reservationsQuery } from '../Reservations'

/**
 * @class Reservation
 * @description Create Reservation page component
 */
export default class Reservation extends Component<any> {
  static navigationOptions = (): NavigationStackScreenOptions => ({
    title: 'RESERVATION'
  })

  onMutationCompleted = () => {
    navigate(ROUTES.Reservations);
  }

  render() {
    return (
      <Mutation
        mutation={createReservationMutation}
        onCompleted={this.onMutationCompleted}
        refetchQueries={[
          reservationsQuery
        ]}
      >
        {(createReservation: Function) => (
          <ReservationDetail
            onSubmit={(data: GQL.IReservationCreateInput) => {
              createReservation({
                variables: {
                  data
                }
              });
            }}
          />
        )}
      </Mutation>
    )
  }
}
