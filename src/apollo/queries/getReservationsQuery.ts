import gql from 'graphql-tag';

const getReservationsQuery = gql`
  query Reservations(
    $where: ReservationWhereInput,
    $orderBy: ReservationOrderByInput,
    $skip: Int,
    $after: String,
    $before: String,
    $first: Int,
    $last: Int
  ) {
    reservations(
      where: $where,
      orderBy: $orderBy,
      skip: $skip,
      after: $after,
      before: $before,
      first: $first,
      last: $last
    ) {
      id
      name
      hotelName
      arrivalDate
      departureDate
    }
  }
`;

export default getReservationsQuery;
