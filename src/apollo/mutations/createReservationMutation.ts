import gql from "graphql-tag";

const createReservationMutation = gql`
  mutation CreateReservation($data: ReservationCreateInput!) {
    createReservation(data: $data) {
      id
      name
      hotelName
      arrivalDate
      departureDate
    }
  }
`;

export default createReservationMutation;
