import {
  createAppContainer,
  createStackNavigator,
  NavigationActions,
  NavigationContainerComponent
} from 'react-navigation';
import ReservationsScreen from '../containers/Reservations';
import ReservationScreen from '../containers/Reservation';
import colors from '../styles/colors';

// Store global navigator instance that can be used in any component
let navigator: NavigationContainerComponent;

// Set global navigator instance
export const setNavigator = (ref: NavigationContainerComponent) => {
  if (ref && !navigator) {
    navigator = ref;
  }
};

// Navigate function that can navigate easily to other route in anywhere
export const navigate = (routeName: string, params?: any) => {
  if (!navigator || !routeName) {
    return;
  }

  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params
    })
  );
};

export enum ROUTES {
  Reservations = 'Reservations',
  Reservation = 'Reservation'
};

const RootStack = createStackNavigator(
  {
    Reservations: ReservationsScreen,
    Reservation: ReservationScreen
  }, {
    initialRouteName: ROUTES.Reservations,
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: colors.primary
      },
      headerTintColor: 'white',
      headerTitleStyle: {
        fontFamily: 'Lato-Bold',
        fontSize: 20
      }
    }
  }
);

const AppContainer = createAppContainer(RootStack);

export default AppContainer;
