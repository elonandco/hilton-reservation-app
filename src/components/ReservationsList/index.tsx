import React, { Component } from 'react';
import { View, FlatList } from 'react-native';
import ReservationItem from './ReservationItem';
import styles from './styles';

interface Props {
  items: Array<IReservation>;
}
/**
 * @class ReservationsList
 * @description Render reservations list with the passed data
 */
export default class ReservationsList extends Component<Props> {
  constructor(props: Props) {
    super(props);
    this.state = {
      selected: new Map()
    }
  }

  keyExtractor = (item: IReservation) => item.id;

  renderItem = ({item}: {item: IReservation}) => (
    <ReservationItem item={item}/>
  )

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.items}
          extraData={this.state}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
        />
      </View>
    )
  }
}
