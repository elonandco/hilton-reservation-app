import React from 'react';
import renderer from 'react-test-renderer';

import ReservationItem from '../index';

describe('ReservationItem component', () => {
  it('renders correctly with the item props', () => {
    const item: GQL.IReservation = {
      __typename: 'Reservation',
      id: 'the-uuid',
      name: 'Test One',
      hotelName: 'Hotel One',
      arrivalDate: '04/20/2019',
      departureDate: '04/22/2019'
    }
    const component = renderer
      .create(<ReservationItem item={item}/>)
      .toJSON();

    expect(component).toMatchSnapshot();
  });
});
