import React, { Component } from 'react';
import { Text, View } from 'react-native';
import styles from './styles';
import colors from '../../../styles/colors';
import getGlobalStyles from '../../../styles/global';

interface Props {
  item: IReservation;
}
/**
 * @class ReservationItem
 * @description Render a reservation item with the passed data
 */
export default class ReservationItem extends Component<Props> {
  render() {
    const globalStyles = getGlobalStyles();
    const { item } = this.props;
    return (
      <View style={styles.itemStyle}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center'}}>
          <Text style={{ ...globalStyles.bodyText, color: colors.primary }}>{item.name}</Text>
          <Text>Arrival: {item.arrivalDate}</Text>
        </View>
        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'center'}}>
          <Text style={{ ...globalStyles.bodyText }}>{item.hotelName}</Text>
          <Text>Departure: {item.departureDate}</Text>
        </View>
      </View>
    );
  }
}
