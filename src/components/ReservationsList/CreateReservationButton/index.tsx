import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '../../../styles/colors';
import { navigate, ROUTES } from '../../../routes';
import styles from './styles';

/**
 * @class CreateReservationButton
 * @description CreateReservation button component which will be rendered on the reservations page header
 */
export default class CreateReservationButton extends Component {
  handleButtonPress = () => {
    navigate(ROUTES.Reservation);
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={this.handleButtonPress}
      >
        <Icon size={20} name='plus' color={colors.primary}/>
      </TouchableOpacity>
    )
  }
}
