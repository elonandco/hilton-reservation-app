import React from 'react';
import renderer from 'react-test-renderer';

import CreateReservationButton from '../index';

jest.mock('../../../../routes', () => {
  return () => ({
    navigate: () => {},
    ROUTES: {
      Reservation: 'Reservation'
    }
  });
});

jest.mock('react-native-vector-icons/Feather', () => 'Icon');

describe('CreateReservationButton component', () => {
  it('renders correctly', () => {
    const component = renderer
      .create(<CreateReservationButton/>)
      .toJSON();

    expect(component).toMatchSnapshot();
  });
});
