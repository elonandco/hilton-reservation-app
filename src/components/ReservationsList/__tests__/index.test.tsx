import React from 'react';
import renderer from 'react-test-renderer';

import ReservationsList from '../index';

describe('ReservationsList component', () => {
  it('renders correctly with the items props', () => {
    const items: Array<IReservation> = [
      {
        id: 'a1',
        name: 'Test One',
        hotelName: 'Hotel 1',
        arrivalDate: '04/20/2019',
        departureDate: '04/21/2019'
      }, {
        id: 'a2',
        name: 'Test Two',
        hotelName: 'Hotel 2',
        arrivalDate: '04/22/2019',
        departureDate: '04/23/2019'
      }
    ];
    const component = renderer
      .create(<ReservationsList items={items}/>)
      .toJSON();

    expect(component).toMatchSnapshot();
  });
});
