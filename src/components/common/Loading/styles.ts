import { StyleSheet } from 'react-native';
import getGlobalStyles from '../../../styles/global';

const globalStyles = getGlobalStyles();

export default StyleSheet.create({
  container: {
    justifyContent: 'center'
  },
  caption: {
    ...globalStyles.caption
  }
});
