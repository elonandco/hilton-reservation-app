import React from 'react';
import renderer from 'react-test-renderer';

import Loading from '../index';

describe('Loading component', () => {
  it('renders correctly with the title props', () => {
    const component = renderer
      .create(<Loading title="Loading data..."/>)
      .toJSON();

    expect(component).toMatchSnapshot();
  });
});
