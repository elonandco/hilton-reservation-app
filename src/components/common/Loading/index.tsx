import React, { Component } from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import colors from '../../../styles/colors';
import styles from './styles';

interface Props {
  title?: string;
}
export default class Loading extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator
          size="large"
          color={colors.primary}
        />
        {this.props.title ? (
          <Text style={styles.caption}>{this.props.title}</Text>
        ) : undefined}
      </View>
    )
  }
}
