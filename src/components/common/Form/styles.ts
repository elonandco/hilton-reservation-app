import { StyleSheet } from 'react-native';
import colors from '../../../styles/colors';

export default StyleSheet.create({
  form: {
    flex: 1,
    padding: 20
  },
  formItem: {
    width: '100%',
    paddingVertical: 10
  },
  formItemLabel: {
    paddingBottom: 5,
    color: colors.darkGrey,
    fontFamily: 'Lato-Regular'
  },
  inputBox: {
    borderColor: colors.lightGrey,
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    fontFamily: 'Lato-Regular'
  },
  datePicker: {
    width: '100%'
  },
  dateInput: {
    width: '100%',
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.lightGrey,
  },
  dateIcon: {
    position: 'absolute',
    top: 8,
    right: 8,
    width: 24,
    height: 24
  },
  primaryButton: {
    backgroundColor: colors.primary,
    padding: 15,
    borderRadius: 5
  },
  buttonText: {
    fontFamily: 'Lato-Heavy',
    fontSize: 16,
    textAlign: 'center'
  }
});
