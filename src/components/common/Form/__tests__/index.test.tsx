import React from 'react';
import renderer from 'react-test-renderer';

import Form from '../index';

describe('Form', () => {
  it('renders correctly with the items props', () => {
    const formItems: Array<IFormItem> = [
      {
        type: 'text',
        name: 'textItem',
        label: 'Text Item',
        placeholder: 'Enter value',
        required: true
      }, {
        type: 'button',
        title: 'SUBMIT',
        submit: true,
        block: true,
        callback: () => {}
      }
    ];
  
    const component = renderer
      .create(<Form items={formItems}/>)
      .toJSON();

    expect(component).toMatchSnapshot();
  });
});
