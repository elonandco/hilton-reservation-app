import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity, Alert } from 'react-native';
import DatePicker from 'react-native-datepicker';
import getGlobalStyles from '../../../styles/global';
import colors from '../../../styles/colors';
import styles from './styles';

const globalStyles = getGlobalStyles();

interface Props {
  // Form fields array, Form will be built as arranged in this array
  items?: Array<IFormItem>;
}
interface State {
  formData: any;
  focusedItem: string;
}
/**
 * @class Form
 * @description JSON-driven Form component. Generate a Form by given form fields array
 */
export default class Form extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      formData: {},
      focusedItem: ''
    }
  }

  // Update formData state with text that user typed
  handleDataInput = (item: IFormItem, text: string) => {
    if (item.name) {
      this.state.formData[item.name] = text
      this.setState({
        formData: {
          ...this.state.formData,
          [item.name]: text
        }
      })
    }
  }

  // Render a Text field
  renderTextItem = (item: IFormItem, index: number) => {
    if (item.name) {
      return (
        <View key={index} style={styles.formItem}>
          {item.label ? (
            <Text style={styles.formItemLabel}>
              {item.label + (item.required ? '*' : '')}
            </Text>
          ) : undefined}
          <TextInput
            style={styles.inputBox}
            placeholder={item.placeholder}
            value={this.state.formData[item.name]}
            onChangeText={text => this.handleDataInput(item, text)}
          />
        </View>
      );
    }
  }

  // Render a date input field and a date picker
  renderDateItem = (item: IFormItem, index: number) => {
    if (item.name) {
      return (
        <View key={index} style={styles.formItem}>
          {item.label ? (
            <Text style={styles.formItemLabel}>
              {item.label + (item.required ? '*' : '')}
            </Text>
          ) : undefined}
          <DatePicker
            date={this.state.formData[item.name]}
            mode='date'
            placeholder={item.placeholder}
            format={item.format || 'MM/DD/YYYY'}
            confirmBtnText='Confirm'
            cancelBtnText='Cancel'
            showIcon={true}
            style={styles.datePicker}
            customStyles={{
              dateIcon: styles.dateIcon,
              dateInput: styles.dateInput
            }}
            onDateChange={date => this.handleDataInput(item, date)}
          />
        </View>
      );
    }
  }

  // Render a button on Form
  renderButtonItem = (item: IFormItem, index: number) => {
    return (
      <View key={index} style={styles.formItem}>
        <TouchableOpacity
          key={index}
          onPress={() => this.handleButtonPress(item)}
          style={[styles.primaryButton, item.block ? globalStyles.block : undefined]}
        >
          <Text style={[styles.buttonText, {color: item.submit ? colors.white : colors.default}]}>{item.title}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  // Run its callback when the button was pressed
  handleButtonPress = (item: IFormItem) => {
    if (item.callback) {
      // Pass the form data to callback function if this is a submit button
      if (item.submit && this.props.items) {
        // Alert the input error if form is not valid and don't run callback function
        const isFormValid = this.props.items
          .filter(o => o.required && o.name)
          .every(o => o.name && this.state.formData[o.name]);
        if (!isFormValid) {
          Alert.alert('Input Error', 'Enter all required fields.');
        } else {
          item.callback(this.state.formData);
        }
      } else {
        item.callback();
      }
    }
  }

  // Render a form item as type
  renderFormItem = (item: IFormItem, index: number) => {
    switch (item.type) {
      case 'text':
        return this.renderTextItem(item, index);
      case 'date':
        return this.renderDateItem(item, index);
      case 'button':
        return this.renderButtonItem(item, index);
    }
  }

  render() {
    return (
      <View style={styles.form}>
        {this.props.items ? (
          this.props.items.map(this.renderFormItem)
        ) : undefined}
      </View>
    )
  }
}
