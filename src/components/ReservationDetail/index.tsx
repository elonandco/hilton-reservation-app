import React, {Component} from 'react';
import Form from '../common/Form';

interface Props {
  onSubmit?: (data: GQL.IReservationCreateInput) => void;
}

/**
 * @class Reservation
 * @description Create Reservation Form component.
 */
export default class Reservation extends Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  handleFormSubmit = (formData: any) => {
    if (this.props.onSubmit) {
      this.props.onSubmit(formData);
    }
  }

  render() {
    const items: Array<IFormItem> = [
      {
        name: 'name',
        type: 'text',
        label: 'Full Name',
        placeholder: 'Enter your name',
        required: true
      }, {
        name: 'hotelName',
        type: 'text',
        label: 'Hotel Name',
        placeholder: 'Enter hotel name',
        required: true
      }, {
        name: 'arrivalDate',
        type: 'date',
        format: 'MM/DD/YYYY',
        label: 'Arrival Date',
        placeholder: 'Pick a date',
        required: true
      }, {
        name: 'departureDate',
        type: 'date',
        format: 'MM/DD/YYYY',
        label: 'Departure Date',
        placeholder: 'Pick a date',
        required: true
      }, {
        type: 'button',
        title: 'SUBMIT RESERVATION',
        submit: true,
        block: true,
        callback: this.handleFormSubmit
      }
    ]

    return (
      <Form items={items}/>
    )
  }
}
