import React from 'react';
import renderer from 'react-test-renderer';

import ReservationDetail from '../index';

describe('ReservationDetail component', () => {
  it('renders correctly', () => {
    const component = renderer
      .create(<ReservationDetail/>)
      .toJSON();

    expect(component).toMatchSnapshot();
  });
});
