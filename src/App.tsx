import React, { Component } from 'react';
import { ApolloProvider } from 'react-apollo';
import { NavigationContainerComponent } from 'react-navigation';
import AppContainer, { setNavigator } from './routes';
import client from './apollo/client';

export default class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <AppContainer ref={(ref: NavigationContainerComponent) => setNavigator(ref)}/>
      </ApolloProvider>
    );
  }
}
