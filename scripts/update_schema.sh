#!/bin/sh

./node_modules/.bin/get-graphql-schema https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev > ./data/schema.graphql
./node_modules/.bin/get-graphql-schema https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev -j > ./data/schema.json
./node_modules/.bin/gql2ts ./data/schema.json -o typings/graphql.d.ts
